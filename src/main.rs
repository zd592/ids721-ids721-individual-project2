use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use individual_project2::process_data; 

async fn index() -> impl Responder {
    let result = process_data(10); // Using the process_data function with a hardcoded input.
    HttpResponse::Ok().body(format!("Processed data: {}", result))
}

#[actix_web::main] // Use Actix's macro to run the server.
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index)) // Set up a route to our index function.
    })
    .bind("127.0.0.1:8080")? // Bind to localhost on port 8080.
    .run()
    .await
}
