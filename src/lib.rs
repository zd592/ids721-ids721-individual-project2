// Define a public function to process data.
// For simplicity, this function will just increment a given number.

pub fn process_data(input: i32) -> i32 {
    input + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    // Unit test for `process_data` function.
    #[test]
    fn test_process_data() {
        assert_eq!(process_data(1), 2);
    }
}

