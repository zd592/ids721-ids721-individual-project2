# individual_project

Provide a brief description of `individual_project2`, including its purpose and any relevant information about what it does.

## Prerequisites

Before starting, ensure you have the following installed on your machine:
- Rust and Cargo (Follow the [official installation guide](https://www.rust-lang.org/tools/install))
- Docker (Refer to the [Docker installation guide](https://docs.docker.com/get-docker/))

## Building the Project

1. **Create the Project** (if starting from scratch):
   ```bash
   cargo new individual_project2 --bin
   ```
   This command creates a new binary project named `individual_project2`.

2. **Build the Project**:
   Navigate to the project directory:
   ```bash
   cd individual_project2
   ```
   Then compile the project with:
   ```bash
   cargo build
   ```

## Running the Project

Start the project using Cargo:
```bash
cargo run
```
This command compiles (if necessary) and runs the `individual_project` application.

## Testing the Project

After the application is running, you can test it by sending a request to the server. Open a new terminal window and execute:
```bash
curl http://127.0.0.1:8080/
```
This command uses `curl` to send an HTTP GET request to the application. Adjust the URL as needed based on your application's routing and port settings.

## Dockerizing the Application

To containerize `individual_project`, follow these steps:

### 1. **Create a Dockerfile**

Create a `Dockerfile` in the root directory of your project with the following content:
```Dockerfile
# Build stage
FROM rust:1.67 as build
WORKDIR /usr/src/individual_project
COPY . .
RUN cargo build --release

# Runtime stage
FROM debian:buster-slim
COPY --from=build /usr/src/individual_project/target/release/individual_project /usr/local/bin/individual_project
EXPOSE 8080
CMD ["individual_project"]
```

### 2. **Build the Docker Image**

From the root directory of your project, run:
```bash
docker build -t individual_project2 .
```
This command builds a Docker image named `individual_project2` from your Dockerfile.

### 3. **Run a Container**

Launch a container from your image:
```bash
docker run -d -p 8080:8080 --name individual_project2_container individual_project2
```
This runs your application in a container named `individual_project2_container`, mapping port 8080 of the container to port 8080 on the host.

### 4. **Viewing Logs**

To check the logs of the running container, use:
```bash
docker logs individual_project2_container
```

### 5. **Stopping and Removing the Container**

Stop the container with:
```bash
docker stop individual_project_container
```
And remove it with:
```bash
docker rm individual_project_container
```

## Screen shots:
-  Test the function
    - ![Alt text](./images/0.jpeg "Optional title0")

-  Build the docker, ps, Docker Desktop
    - ![Alt text](./images/1.jpeg "Optional title1")
    - ![Alt text](./images/2.jpeg "Optional title2")
    - ![Alt text](./images/3.jpeg "Optional title3")
